from elevator.Scheduler import Scheduler
from elevator.Building import Building
from elevator.Config import Config
from elevator.Utilities import Utilities


def main():
    ## SINGLE OPERATION MODE
    a_building = Building(
        building_name=Config.building_name,
        number_of_elevators=Config.elevators_number_of,
        number_of_people=Config.building_number_of_people,
        number_of_floors=Config.building_number_of_floors,
        population_mode=Config.population_mode)

    elevator_scheduler = Scheduler(
        building=a_building,
        scheduler_mode=Config.scheduler_mode,
        day_length_in_minutes=Config.scheduler_day_length_in_minutes)

    elevator_scheduler.run()

if __name__ == '__main__':
    main()
