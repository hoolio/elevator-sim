import random
import time
import numpy as np

class Utilities:
    @staticmethod
    def determine_direction(source_floor, target_floor):
        direction = None

        if source_floor > target_floor:
            direction = 'down'
        # going up
        if source_floor < target_floor:
            direction = 'up'
        # not moving ..
        if source_floor == target_floor:
            direction = None

        return direction

    @staticmethod
    def pick_random_line_from_file(filename):
        lines = open(filename).read().splitlines()
        random_line = random.choice(lines)
        return random_line

    @staticmethod
    def generate_unique_random_start_and_dest_floors(building):
        # generate random start and dest floors
        source_floor = random.randint(0, building.number_of_floors)
        destination_floor = random.randint(0, building.number_of_floors)

        # if they're the same, regenerate the destination_floor
        while source_floor == destination_floor:
            destination_floor = random.randint(0, building.number_of_floors)

        return source_floor, destination_floor

    @staticmethod
    def create_list_from_range(r1, r2):
        return [item for item in range(r1, r2 + 1)]

    @staticmethod
    def rotate_list_by_n(input_list, rotate_by):
        #output_list = input_list[rotate_by:] + input_list[:rotate_by]

        # from https://www.geeksforgeeks.org/python-program-right-rotate-list-n/
        output_list = []
        # Will add values from n to the new list
        for item in range(len(input_list) - rotate_by, len(input_list)):
            output_list.append(input_list[item])

        # Will add the values before
        # n to the end of new list
        for item in range(0, len(input_list) - rotate_by):
            output_list.append(input_list[item])

        return output_list

    @staticmethod
    def current_milli_time():
        return round(time.time() * 1000)

    @staticmethod
    def split_list_into_n_parts(list, number_of_parts):
        # from https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
        return np.array_split(list, number_of_parts)




