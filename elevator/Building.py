import json
import sys

from elevator.Elevator import Elevator
from elevator.Person import Person
from elevator.Utilities import Utilities
from elevator.Config import Config
from elevator.Plogger import Plogger


class Building:
    def __init__(self, number_of_elevators, number_of_people,
                 number_of_floors, building_name=None, population_mode=None):

        self.plogger = Plogger(
            plogger_type=Config.plogger_type,
            plogging_enabled=Config.plogging_enabled,
            filter_level=Config.plogger_filter_level)

        self.building_name = building_name
        self.number_of_floors = number_of_floors
        self.floor_list = Utilities.create_list_from_range(0, number_of_floors)
        self.number_of_elevators = number_of_elevators

        self.population_mode = population_mode
        self.number_of_people = number_of_people

        self.people = self.init_people()

        self.elevators = self.init_elevators(self.number_of_elevators)

        self.unsatisfied_people = []

    def __repr__(self):
        return "Building(name:{}, floors:{}, elevators:{}, people:{}, pop_mode:{})"\
            .format(self.building_name,
                    self.number_of_floors,
                    self.number_of_elevators,
                    len(self.people),
                    self.population_mode)

    # makes number_of_elevators elevators and returns them as a list
    def init_elevators(self, number_of_elevators):
        created_elevators = []
        for number in range(number_of_elevators):
            an_elevator = Elevator(self)
            created_elevators.append(an_elevator)
        return created_elevators

    def init_people(self):
        assert self.population_mode in ['static', 'dynamic']

        if self.population_mode == 'static':
            self.people = self.read_people_from_json()

        if self.population_mode == 'dynamic':
            self.people = self.init_random_people()
            self.write_people_to_json()

        self.number_of_people = len(self.people)

        return self.people

    def init_random_people(self):
        assert self.number_of_people > 0
        created_people = []

        while len(created_people) < self.number_of_people:
            a_people = Person(self)
            if a_people.name not in created_people:
                created_people.append(a_people)

        return created_people

    def calculate_unsatisfied_people(self):
        self.unsatisfied_people = []

        for person in self.people:
            if person.satisfied is False:
                self.unsatisfied_people.append(person)

        return self.unsatisfied_people

    def write_people_to_json(self):

        list_of_dicts = []
        for person in self.people:
            a_person_dict = {
                "name": person.name,
                "source_floor": person.source_floor,
                "destination_floor": person.destination_floor
            }
            list_of_dicts.append(a_person_dict)

        json_object = json.dumps(list_of_dicts, indent=4)

        try:
            with open(Config.people_as_json_file, "w") as outfile:
                outfile.write(json_object)
            self.plogger.ploggit('debug', "Saved {} people to {}"
                                 .format(len(list_of_dicts), Config.people_as_json_file))
        except Exception as e:
            self.plogger.ploggit('error', "Unable to save to {}!".format(Config.people_as_json_file))
            self.plogger.ploggit('error', "{}!".format(e))

            sys.exit(1)

    def read_people_from_json(self):
        # delete current list of people
        created_people = []

        try:
            f = open(Config.people_as_json_file)
            list_of_people_dicts = json.load(f)
            self.plogger.ploggit('debug', "Read {} people from {}"
                                 .format(len(list_of_people_dicts), Config.people_as_json_file))
        except Exception as e:
            self.plogger.ploggit('error', "Unable to read from {}!".format(Config.people_as_json_file))
            self.plogger.ploggit('error', "{}!".format(e))

            sys.exit(1)

        for person_from_file in list_of_people_dicts:
            person = Person(self)
            person.name = person_from_file["name"]
            person.source_floor = person_from_file["source_floor"]
            person.destination_floor = person_from_file["destination_floor"]

            created_people.append(person)

        return created_people
