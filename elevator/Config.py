# TODO track versions properly using semver

class Config:
    # APP NAME
    app_name = "A curious elevator simulator"
    app_version = '2.0.2'

    ## ELEVATOR
    elevators_number_of = 2
    elevator_travel_time_seconds = 0.00
    elevator_capacity = 15  # ie how many people can fit inside

    # BUILDING
    # population_mode is detailed in README.md
    population_mode = 'static' # reads population from json
    # population_mode = 'dynamic' # uses building_people_number_of
    building_number_of_people = 40

    building_number_of_floors = 10
    building_name = 'Tiny Tower'

    people_as_json_file = './elevator/people.json'

    # SCHEDULER
    # elevators run all day long (or until demand is satisfied?)
    scheduler_day_length_in_minutes = 60
    # scheduler_mode is detailed in README.md
    #scheduler_mode = 'dumb_dumb'
    scheduler_mode = 'dumb_interleaved'
    # NOT IMPLEMENTED YET
    # scheduler_mode = 'demand'

    ## PEOPLE
    people_names_list_file = './elevator/assets/names.txt'

    ## LOGGING/OUTPUT
    # maybe leave plogger_type at type3, or have a look at Plogger.py
    plogging_enabled = True
    plogger_filter_level = 'debug'
    plogger_type = 'type4'
