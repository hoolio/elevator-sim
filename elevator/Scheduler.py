from elevator.Utilities import Utilities
import sys
import numpy as np

# TODO review logic in README and implement scheduler demand mode


class Scheduler:
    def __init__(self, building, scheduler_mode, day_length_in_minutes):
        self.building = building
        self.plogger = self.building.plogger

        #assert scheduler_mode in ['dumb_dumb', 'dumb_interleaved', 'demand']
        assert scheduler_mode in ['dumb_dumb', 'dumb_interleaved']

        self.scheduler_mode = scheduler_mode
        self.schedule_move_cost = 0
        self.schedule_accumulated_time_cost = 0
        self.unsatisfied_people = 0

        self.day_length_in_minutes = day_length_in_minutes
        self.time = 0

    def __repr__(self):
        return "Scheduler(mode:{}, unsatisfied_people:{} move_cost:{}, time_cost:{})" \
            .format(self.scheduler_mode,
                    self.unsatisfied_people,
                    self.schedule_move_cost,
                    self.schedule_accumulated_time_cost)

    def run(self):
        ## INFO
        self.plogger.ploggit('info', self.building)
        self.plogger.ploggit('info', "Queued people:{}".format(len(self.building.calculate_unsatisfied_people())))

        # GENERATE THE SCHEDULE FOR EACH ELEVATOR
        self.generate_schedules()

        # RUN THE SCHEDULER
        self.plogger.ploggit('info', "Running {}".format(self))
        # this is a for loop where we iterate minutes in the day
        for minutes in range(self.day_length_in_minutes):
            self.time = minutes
            for elevator in self.building.elevators:
                # sends the elevator to a valid floor even when minutes > len elevator.schedule
                elevator.change_floor(elevator.schedule[self.time % len(elevator.schedule)])
                self.plogger.ploggit('debug', "time:{} {}".format(self.time, elevator))

            # EXIT EARLY, NO DEMAND?
            if len(self.building.unsatisfied_people) == 0:
                self.calculate_costs()
                self.plogger.ploggit('info', "Finished (no demand) {}".format(self))
                sys.exit(0)

        # WE RAN OUT OF TIME
        self.calculate_costs()
        self.plogger.ploggit('warning', "Not finished (no time) {}".format(self))

    def generate_schedules(self):
        self.plogger.ploggit('info', "Generating schedules using {} mode".format(self.scheduler_mode))

        for elevator in self.building.elevators:
            match self.scheduler_mode:
                case 'dumb_dumb':
                    elevator.schedule = self.create_dumb_dumb_schedule()
                case 'dumb_interleaved':
                    elevator.schedule = self.create_dumb_interleaved_schedule(elevator.id)

            self.plogger.ploggit('debug', "elevator {} schedule {}".format(elevator.name, elevator.schedule))

    def create_dumb_interleaved_schedule(self, id):
        plain_up_down_schedule = self.create_dumb_dumb_schedule()

        # id is the id of the elevator in question, NOT zero indexed!
        if id == 1:
            return plain_up_down_schedule
        else:
            # rotate_by gets a bit crud after 2 elevators.  it works though.
            rotate_by = int(len(plain_up_down_schedule) / id)
            dumb_interleaved_schedule = Utilities.rotate_list_by_n(plain_up_down_schedule, rotate_by)

            return dumb_interleaved_schedule

    def create_dumb_dumb_schedule(self):
        # generate schedule as a list eg [0, 1, 2, 3, 4, 5, 5, 4, 3, 2, 1, 0]
        list_from_range = Utilities.create_list_from_range(0, self.building.number_of_floors)
        plain_up_down_schedule = list_from_range
        plain_up_down_schedule.extend(reversed(list_from_range))

        # return as np as array so it prints nicely next to actual np arrays used elsewhere
        return np.array(plain_up_down_schedule)

    def calculate_costs(self):
        self.unsatisfied_people = len(self.building.calculate_unsatisfied_people())

        if self.unsatisfied_people > 0:
            self.plogger.ploggit('warning', "Unsatisfied people: {}".format(self.unsatisfied_people))
            for people in self.building.calculate_unsatisfied_people():
                self.plogger.ploggit('debug', "{}".format(people))

        for elevator in self.building.elevators:
            self.schedule_move_cost = self.schedule_move_cost + elevator.move_count

        for person in self.building.people:
            self.schedule_accumulated_time_cost = self.schedule_accumulated_time_cost + person.wait_time
