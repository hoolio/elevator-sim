
from elevator.Config import Config
from elevator.Utilities import Utilities


class Person:
    def __init__(self, building):
        self.building = building
        self.plogger = self.building.plogger

        self.name = Utilities.pick_random_line_from_file(Config.people_names_list_file)
        self.creation_time = Utilities.current_milli_time()
        self.wait_time = 0

        # ensure source and dest floors are unique
        self.source_floor, self.destination_floor = Utilities.generate_unique_random_start_and_dest_floors(building)

        # do we actually need to process their movement?
        self.satisfied = self.calculate_self_satisfied()

        self.direction = Utilities.determine_direction(self.source_floor, self.destination_floor)

    def __repr__(self):
        return "Person(name:{}, src:{}, dest:{}, direction:{})" \
            .format(self.name, self.source_floor, self.destination_floor, self.direction)

    def calculate_self_satisfied(self):
        if self.source_floor == self.destination_floor:
            return True
        else:
            return False

    def calculate_wait_time(self):
        end_time = Utilities.current_milli_time()

        self.wait_time = end_time - self.creation_time

        # print("..start_time:{}, end_time:{}, wait_time:{}".format(self.creation_time, end_time, self.wait_time))

        return self.wait_time
