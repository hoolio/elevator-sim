import uuid
import time
from elevator.Config import Config
from elevator.Utilities import Utilities


# noinspection PyTypeChecker
class Elevator:
    instance_counter = 0
    def __init__(self, building):
        self.building = building
        self.plogger = self.building.plogger

        Elevator.instance_counter = Elevator.instance_counter + 1
        self.id = Elevator.instance_counter
        self.name = "ele_" + str(uuid.uuid4())[:3]
        self.current_floor = 0  # start on ground floor
        self.passengers = []
        #self.going_down = None
        self.target_floor = None
        self.schedule = []
        self.move_count = 0
        self.this_move_cost = None
        self.direction = None
        #self.stopped = False

    def __repr__(self):
        return "Elevator(id:{}, name:{}, floor:{}, target:{} dir:{}, passengers:{})" \
            .format(self.id, self.name, self.current_floor, self.target_floor, self.direction, len(self.passengers))

    def change_floor(self, target_floor):
        # set elevator direction.
        self.target_floor = target_floor
        self.direction = Utilities.determine_direction(self.current_floor, self.target_floor)
        self.this_move_cost = abs(self.current_floor - self.target_floor)

        # move the elevator
        self.current_floor = self.target_floor
        # sleep proportional to the number of floors being traversed
        time.sleep(self.this_move_cost * Config.elevator_travel_time_seconds)

        # process passengers
        self.add_passenger()
        self.remove_passenger()

        # calculate move cost
        self.move_count = self.move_count + self.this_move_cost

    def add_passenger(self):
        for passenger in self.building.calculate_unsatisfied_people():
            # if the negated condition matches we skip that passenger
            #
            # are the elevator and passenger on the same floor?
            if self.current_floor != passenger.source_floor:
                continue

            # ensure travel direction is the same
            if self.direction != passenger.direction and self.direction is not None:
                continue

            # make sure we don't add them twice!!
            if passenger in self.passengers:
                continue

            # make sure the elevator isn't full
            if len(self.passengers) > Config.elevator_capacity:
                continue

            # actually do the thing
            self.passengers.append(passenger)

    def remove_passenger(self):
        # the [:] means we're working on a copy of the list, so we can modify the original
        # this bug took a very long time to find, so I'm leaving this note here as a reminder.
        for passenger in self.passengers[:]:
            if self.current_floor == passenger.destination_floor:

                # update passenger stats
                passenger.satisfied = True
                passenger.calculate_wait_time()

                self.passengers.remove(passenger)
