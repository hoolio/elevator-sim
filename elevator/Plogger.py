from elevator.Config import Config
from elevator.Utilities import Utilities
import inspect
import os


class Plogger:
    def __init__(self, plogger_type, plogging_enabled, filter_level):
        self.plogging_enabled = plogging_enabled
        self.filter_level = filter_level
        self.plogger_type = plogger_type

        self.possible_log_levels = ["error", "warning", "info", "debug"]
        self.possible_plogger_types = ["type1", "type2", "type3", "type4"]

        assert filter_level in self.possible_log_levels
        assert plogger_type in self.possible_plogger_types

        # STARTUP BANNER
        self.ploggit('info', "{} v{}".format(Config.app_name, Config.app_version))

    def ploggit(self, loglevel, message):
        assert loglevel in self.possible_log_levels

        # don't do anything unless plogger is enabled
        if not self.plogging_enabled:
            return 0

        # FILTER BASED ON LOGLEVEL
        # here we find index of the loglevel for direct comparison
        configured_loglevel = self.possible_log_levels.index(Config.plogger_filter_level)
        item_loglevel = self.possible_log_levels.index(loglevel)
        # now don't do anything if the log_levels don't match(?)
        if configured_loglevel < item_loglevel:
            return 0

        ## DEBUG VARS
        calling_function = inspect.stack()[1].function
        calling_file = os.path.basename(inspect.stack()[1].filename)
        lineno = inspect.stack()[1].lineno


        ## PLOGGIT!
        if self.plogger_type == 'type1':
            print("{}: {}() in {} says: {}".format(loglevel, calling_function, calling_file, message))

        if self.plogger_type == 'type2':
            print("{}: {}() line {} says: {}".format(loglevel, calling_function, lineno, message))

        if self.plogger_type == 'type3':
            print("{} {}: {}".format(loglevel, Utilities.current_milli_time(), message))

        if self.plogger_type == 'type4':
            print("{}: {}".format(loglevel, message))
