# What is this?
While this is called an elevator simulator it actually models a Building with various parameters
(number of floors, number of people, number of elevators etc.) and models the operation of the elevators
in various operation modes as orchestrated by the Scheduler.

## Configuration
All config items are specified in Config.py

### Building and population modes
People spawn within the building depending on the population mode set in the config file.

`population_mode = dynamic`:
1. People spawn randomly within the building and have a unique source and destination floor.
2. People attributes, name, source and destination floors are stored in a json file for later use.

`population_mode = static`:
1. As per `dynamic` however People are loaded from the json file.
2. In this case `building_people_number_of` is ignored.

People are satisfied when they reach their destination floor.
There is no biasing of people to or from the ground floor (floor 0).
All people are instantiated as soon as the simulation begins.
The building has other config items, such as name, number of floors, elevator capacity etc.

### Scheduler operation modes
`scheduler_mode = dumb_dumb`:
1. Only first elevator operates, others are ignored.
2. Elevator moves consecutively starting from floor 0 (ground) to the top and back to 0.
3. Traverses the building once, up and down, once, that's it.

`scheduler_mode = dumb_interleaved`:
1. All elevators operate
2. Elevators move to every floor consecutively
3. Elevators run schedules roughly opposite each other
4. Elevators simply stop operating when all passengers are satisfied

### Output
Output is controlled by the Plogger class, which works like the python logging package.
All events have a loglevel and the Config allows you to specify which you see on the console.
Possible log levels are ["error", "warning", "info", "debug"] where later log levels include
all previous levels; eg "info" includes "info", "warning" and "error"

## Installation
Built with python 3.10 and pycharm.  Currently, no external packages are required.

## Running
Run `main.py` and output will look something like this.  Play with the config file.
```
info 1663108249308: A curious elevator simulator v1.6.0
info 1663108249338: Building(name:Tiny Tower, floors:10, elevators:1, people:50, pop_mode:static)
info 1663108249339: Queued people:50
info 1663108249339: Running Scheduler(mode:dumb_dumb, unsatisfied_people:0 move_cost:0, time_cost:0)
info 1663108249339: Elevator(name:ele_c0, floor:0, target:0 going_down:None, passengers:4
info 1663108249339: Elevator(name:ele_c0, floor:1, target:1 going_down:False, passengers:5
info 1663108249339: Elevator(name:ele_c0, floor:2, target:2 going_down:False, passengers:7
info 1663108249339: Elevator(name:ele_c0, floor:3, target:3 going_down:False, passengers:9
info 1663108249340: Elevator(name:ele_c0, floor:4, target:4 going_down:False, passengers:9
info 1663108249340: Elevator(name:ele_c0, floor:5, target:5 going_down:False, passengers:12
info 1663108249340: Elevator(name:ele_c0, floor:6, target:6 going_down:False, passengers:13
info 1663108249340: Elevator(name:ele_c0, floor:7, target:7 going_down:False, passengers:15
info 1663108249340: Elevator(name:ele_c0, floor:8, target:8 going_down:False, passengers:13
info 1663108249341: Elevator(name:ele_c0, floor:9, target:9 going_down:False, passengers:14
info 1663108249341: Elevator(name:ele_c0, floor:10, target:10 going_down:False, passengers:9
info 1663108249341: Elevator(name:ele_c0, floor:10, target:10 going_down:None, passengers:11
info 1663108249341: Elevator(name:ele_c0, floor:9, target:9 going_down:True, passengers:12
info 1663108249341: Elevator(name:ele_c0, floor:8, target:8 going_down:True, passengers:15
info 1663108249342: Elevator(name:ele_c0, floor:7, target:7 going_down:True, passengers:16
info 1663108249342: Elevator(name:ele_c0, floor:6, target:6 going_down:True, passengers:15
info 1663108249342: Elevator(name:ele_c0, floor:5, target:5 going_down:True, passengers:14
info 1663108249342: Elevator(name:ele_c0, floor:4, target:4 going_down:True, passengers:14
info 1663108249342: Elevator(name:ele_c0, floor:3, target:3 going_down:True, passengers:10
info 1663108249343: Elevator(name:ele_c0, floor:2, target:2 going_down:True, passengers:11
info 1663108249343: Elevator(name:ele_c0, floor:1, target:1 going_down:True, passengers:9
info 1663108249343: Elevator(name:ele_c0, floor:0, target:0 going_down:True, passengers:8
warning 1663108249343: Unsatisfied people: 18
info 1663108249343: Finished Scheduler(mode:dumb_dumb, unsatisfied_people:18 move_cost:20, time_cost:593)
```

# Architecture
All entities are modelled as Classes and interact with each other in pythonic object-oriented ways.

# Buy why??
Some history: when I went to uni, the second year software engineering lecturer was from India and I
couldn't understand anything he said.  I like to think he was otherwise good at his job.  Regardless
and perhaps due to lack of application on my behalf, my software engineering career tanked when we
were assigned a project to implement an elevator simulator.  20+ years later and through various means
improved at software engineering and so this project is kinda one of my bucket list items, it's silly,
but I want to do it well do prove to myself I can.

Any details on the exact specification of this assignment would be greatly appreciated.

# Thanks to
1. pycharm which _dramatically_ improved my apparent coding abilities.
2. Corey Schafer who produces BRILLIANT tutorial material, free on YouTube.  Consider joining his Patreon.

# In the future??:
`scheduler_mode = demand`:
1. Elevators are allocated a share of the unsatisfied passengers
2. Elevators create their own demand queue from passenger source and destination floors
3. Their demand queues are elegantly sorted and duplicates are removed.
4. Moves are made according to the schedule and stops when its schedule is empty.
NOTE: passenger destination floor can't be sorted in the schedule before their target!
